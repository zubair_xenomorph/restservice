package com.evolint.restservice;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText temp_ed_txt;
    private EditText btry_ed_txt;
    private Button send_serv;
    private final String URL = "https://kzubair.pythonanywhere.com/rumy/default/index?";
    private String URI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        temp_ed_txt=(EditText)findViewById(R.id.temp_txt);
        btry_ed_txt=(EditText)findViewById(R.id.bat_txt);
        temp_ed_txt.setText("0");
        btry_ed_txt.setText("0");
        send_serv=(Button)findViewById(R.id.send_d);
        send_serv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                URI=new String(URL+"temp="+temp_ed_txt.getText()+"&bat="+btry_ed_txt.getText());
                send_to_url(URI);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.exit){
            finish();
            System.exit(0);
        }
        return false;
    }

    private void send_to_url(String u) {
        if(isOnline())
        requestData(u);
        else {
            Toast.makeText(this, "Network is not available", Toast.LENGTH_LONG).show();
        }
    }

    private void requestData(String url) {
        MyTask tsk = new MyTask();
        tsk.execute(url);
    }



    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo!=null && netInfo.isConnectedOrConnecting()){
            return true;
        }
        else{
            return false;
        }
    }

    private class MyTask extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String content = HttpManager.send(params[0]);

            return content;
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }
}
