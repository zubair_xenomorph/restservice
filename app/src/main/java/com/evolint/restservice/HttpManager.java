package com.evolint.restservice;

import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Zubair on 6/29/2016.
 */
public class HttpManager {
    public static String send(String uri){
        BufferedReader reader = null;

        try{
            Log.d("zubair", uri);
            URL url =new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while((line = reader.readLine())!=null){
                sb.append(line + "\n");
            }
            return sb.toString();

        } catch (Exception e){
            Log.d("zubair",e.toString());
            e.printStackTrace();
            return null;

        }finally {
            if(reader!=null){
                try{
                    reader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
